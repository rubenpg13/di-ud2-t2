package es.palacios.g;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Ex3AnchorPane extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        AnchorPane anchorPane = new AnchorPane();
        Button top = new Button();
        top.setText("Botó 1: Anclat a top + left");
        Button left = new Button();
        left.setText("Botó 2: Anclat a top + left + right");
        Button right = new Button();
        right.setText("Botó 3: Anclat a top + right");
        Button bottom = new Button();
        bottom.setText("Botó 4: Anclat a top + left + right + bottom");

        AnchorPane.setTopAnchor(top,40d);
        AnchorPane.setLeftAnchor(top,50d);

        AnchorPane.setTopAnchor(left,90d);
        AnchorPane.setLeftAnchor(left,10d);
        AnchorPane.setRightAnchor(left,320d);

        AnchorPane.setTopAnchor(right,100d);
        AnchorPane.setRightAnchor(right,20d);

        AnchorPane.setTopAnchor(bottom,150d);
        AnchorPane.setLeftAnchor(bottom,40d);
        AnchorPane.setRightAnchor(bottom,50d);
        AnchorPane.setBottomAnchor(bottom,45d);

        anchorPane.getChildren().addAll(top,left,right,bottom);
        Scene scene = new Scene(anchorPane, 800, 300);
        stage.setTitle("Anchor Pane");
        stage.setScene(scene);
        stage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }
}
