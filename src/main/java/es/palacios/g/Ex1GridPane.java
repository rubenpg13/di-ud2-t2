package es.palacios.g;

import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * JavaFX App
 */
public class Ex1GridPane extends Application {

    @Override
    public void start(Stage stage) {
        GridPane gridPane = new GridPane();

        gridPane.setPadding(new Insets(20, 0, 20, 20));

        gridPane.setVgap(7);
        gridPane.setHgap(7);

        gridPane.setAlignment(Pos.CENTER);

        Label label1 = new Label("Nom:");
        GridPane.setHalignment(label1, HPos.RIGHT);
        Label label2 = new Label("Cognom:");
        GridPane.setHalignment(label2, HPos.RIGHT);
        Label label3 = new Label("Ciutat:");
        GridPane.setHalignment(label3, HPos.RIGHT);
        Label label4 = new Label("Direccio:");
        GridPane.setHalignment(label4, HPos.RIGHT);
        Label label5 = new Label("Descripcio:");
        GridPane.setHalignment(label5, HPos.RIGHT);


        TextField textField1 = new TextField();
        TextField textField2 = new TextField();
        TextField textField3 = new TextField();
        TextField textField4 = new TextField();
        TextField textField5 = new TextField();

        gridPane.add(label1,0,0);
        gridPane.add(label2,0,1);
        gridPane.add(label3,0,2);
        gridPane.add(label4,0,3);
        gridPane.add(label5,0,4);
        gridPane.add(textField1, 1, 0);
        gridPane.add(textField2, 1, 1);
        gridPane.add(textField3, 1, 2);
        gridPane.add(textField4, 1, 3);
        gridPane.add(textField5, 1, 4);

        Scene scene = new Scene(gridPane);
        stage.setTitle("Grid Pane");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}