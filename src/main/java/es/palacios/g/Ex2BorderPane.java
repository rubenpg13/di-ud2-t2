package es.palacios.g;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Ex2BorderPane extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        BorderPane borderPane = new BorderPane();
        borderPane.setPadding(new Insets(15,20,10,10));
        Button top = new Button("Element situat en la part superior(Top)");
        top.setPadding(new Insets(10,10,10,10));
        Button left = new Button("Element situat en la part esquerra(Left)");
        left.setPadding(new Insets(10,10,10,10));
        Button right = new Button("Element situat en la part dreta(Right)");
        right.setPadding(new Insets(10,10,10,10));
        Button center = new Button("Element situat en el centre(Center)");
        center.setPadding(new Insets(10,10,10,10));
        Button bottom = new Button("Element situat en la part inferior(Bottom)");
        bottom.setPadding(new Insets(10,10,10,10));

        borderPane.setTop(top);
        borderPane.setLeft(left);
        borderPane.setRight(right);
        borderPane.setCenter(center);
        borderPane.setBottom(bottom);

        BorderPane.setMargin(top,new Insets(10,10,10,10));
        BorderPane.setMargin(left,new Insets(10,10,10,10));
        BorderPane.setMargin(center,new Insets(10,10,10,10));
        BorderPane.setMargin(right,new Insets(10,10,10,10));
        BorderPane.setMargin(bottom,new Insets(10,10,10,10));

        Scene scene = new Scene(borderPane, 800, 300);
        stage.setTitle("Border Pane");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
