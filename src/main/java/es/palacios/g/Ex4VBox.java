package es.palacios.g;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class Ex4VBox extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        VBox vBox = new VBox();
        vBox.setPadding(new Insets(20,20,20,20));
        Label label = new Label("Etiqueta VBox");
        Button button1 = new Button("Button 1 VBox");
        button1.setMinSize(125,125);
        Button button2 = new Button("Button 2 VBox");
        TextField textField = new TextField("TextField VBox");
        RadioButton radioButton = new RadioButton("RadioButton VBox");
        CheckBox checkBox = new CheckBox("CheckBox VBox");
        vBox.getChildren().addAll(label,button1,button2,textField,radioButton,checkBox);

        Scene scene = new Scene(vBox, 800, 300);
        stage.setTitle("VBox");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
